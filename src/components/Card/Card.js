import React from 'react';
import PropTypes from 'prop-types';
import './card.scss';

const Card = (props) => {
  const { children, className } = props;
  return <article className={`Card ${className}`}>{children}</article>;
};

Card.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element.isRequired,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
  className: PropTypes.string,
};

Card.defaultProps = {
  className: '',
};

export default Card;
