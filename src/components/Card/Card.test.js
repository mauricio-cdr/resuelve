import React from 'react';
import { shallow } from 'enzyme';
import Card from './Card';

describe('<Card />', () => {
  it('Should renders without crashing', () => {
    const component = shallow(
      <Card><div>1</div></Card>,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should have Card class', () => {
    const component = shallow(
      <Card><div>1</div></Card>,
    );
    expect(component.find('.Card')).toHaveLength(1);
  });
});
