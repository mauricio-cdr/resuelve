import React, { useState, Suspense } from 'react';
import { useTranslation } from 'react-i18next';
import Switch from './ToggleButton';

const SwitchL = () => {
  const { i18n } = useTranslation();
  const [lang, changeLang] = useState('es');

  const handleChangeLang = () => {
    const newOption = lang === 'es' ? 'en' : 'es';
    changeLang(newOption);
    i18n.changeLanguage(newOption);
  };
  return (
    <Switch selected={lang === 'en'} onChange={handleChangeLang} name="lang" color="primary" />
  );
};

const LangSwitch = () => (
  <div>
    <Suspense fallback={<Switch disabled />}>
      <SwitchL />
    </Suspense>
  </div>
);

export default LangSwitch;
