import React from 'react';
import TButtonM from '@material-ui/lab/ToggleButton';
import CheckIcon from '@material-ui/icons/Check';
import useStyles from './styles';

const ToggleButton = (props) => {
  const classes = useStyles();

  return (
    <TButtonM {...props} classes={{ root: classes.root }}>
      <CheckIcon />
    </TButtonM>
  );
};

export default ToggleButton;
