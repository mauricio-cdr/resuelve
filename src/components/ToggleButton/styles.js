import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
  root: {
    marginRight: '6px',
  },
});
