import React from 'react';
import InputM from '@material-ui/core/TextField';

const Input = (props) => (
  <InputM {...props} />
);

export default Input;
