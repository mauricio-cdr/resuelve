import React from 'react';
import CheckboxM from '@material-ui/core/Checkbox';

const Checkbox = (props) => (
  <CheckboxM {...props} />
);

export default Checkbox;
