import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { AppBar, Toolbar } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import Menu from '@material-ui/core/Menu';
import { useDispatch, useSelector } from 'react-redux';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';
import ToggleButton from '../ToggleButton';
import LangToggleButton from '../ToggleButton/LangToggleButton';
import { logout } from '../../containers/Login/login.actions';
import Text from '../Text';
import useStyles from './styles';
import version from '../../versionName.json';

const ToolbarP = (props) => {
  const { onChangeTheme, isLightThemeSelected } = props;
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    setAnchorEl(null);
    dispatch(logout()).then(() => {
      window.location.reload();
    });
  };

  const classes = useStyles();
  const userInfo = useSelector((state) => state.get('loginState').userInfo);
  return (
    <AppBar position="static">
      <Toolbar variant="dense">
        <Link to="/login" className={classes.link}>
          <Text variant="h2" component="span" style={{ color: 'white' }} translationKey="title" />
        </Link>
        <div style={{ flexGrow: '1' }} />
        <IconButton
          edge="start"
          color="inherit"
          aria-label="open drawer"
          onClick={handleClick}
        >
          <MenuIcon />
        </IconButton>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <span style={{ fontSize: '8px', margin: '0px 15px' }}>
            {version.version_name}
          </span>
          { (userInfo && userInfo.uid)
            && (
            <div className={classes.infoContent}>
              <div>
                <Text translationKey="nav-bar.name-label" />
                {`${userInfo.nombre} ${userInfo.apellido}`}
              </div>
              <div>
                <Text translationKey="nav-bar.role-label" />
                {`${userInfo.admin ? 'Administrador' : 'Usuario'}`}
              </div>
            </div>
            )}
          <Divider />
          <MenuItem onClick={handleClose}>
            <ToggleButton
              selected={isLightThemeSelected}
              onChange={onChangeTheme}
              name="theme"
              value="e"
              color="primary"
            />
            <Text
              variant="body1"
              component="span"
              color="primary"
              translationKey="nav-bar.second-theme"
            />
          </MenuItem>
          <MenuItem onClick={handleClose}>
            <LangToggleButton />
            <Text
              variant="body1"
              component="span"
              color="primary"
              translationKey="nav-bar.english-lang-selected"
            />
          </MenuItem>
          {(userInfo && userInfo.uid)
            && (
            <MenuItem onClick={handleLogout}>
              <Text
                variant="body1"
                component="span"
                color="primary"
                translationKey="nav-bar.logout"
              />
            </MenuItem>
            )}
        </Menu>
      </Toolbar>
    </AppBar>
  );
};

ToolbarP.propTypes = {
  onChangeTheme: PropTypes.func.isRequired,
  isLightThemeSelected: PropTypes.bool.isRequired,
};

export default ToolbarP;
