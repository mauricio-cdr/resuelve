import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
  link: {
    textDecoration: 'none',
  },
  infoContent: {
    margin: '15px',
    marginBottom: '20px',
  },
});
