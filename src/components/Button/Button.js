import React from 'react';
import ButtonM from '@material-ui/core/Button';
import useStyles from './styles';

const Button = (props) => {
  const classes = useStyles();
  return (
    <ButtonM {...props} classes={{ root: classes.root }} />
  );
};
export default Button;
