import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  col: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
});
