import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import useStyles from './styles';

export const LodingPage = () => {
  const classes = useStyles();

  return (
    <div className={classes.row}>
      <div className={classes.col}>
        <CircularProgress variant="indeterminate" size={40} thickness={12} />
      </div>
    </div>
  );
}