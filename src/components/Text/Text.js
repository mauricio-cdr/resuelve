import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Skeleton from 'react-loading-skeleton';
import { useTranslation } from 'react-i18next';


const Translator = (props) => {
  const {
    translationKey, component, variant, ...others
  } = props;
  const { t } = useTranslation('');
  if (component === 'none') {
    return t(`${translationKey}`);
  }
  return (
    <Typography variant={variant} component={component} {...others}>
      {t(`${translationKey}`)}
    </Typography>
  );
};

Translator.propTypes = {
  translationKey: PropTypes.string,
  component: PropTypes.string,
  variant: PropTypes.string,
};

Translator.defaultProps = {
  translationKey: null,
  component: null,
  variant: null,
};


const Text = (props) => (
  <Suspense fallback={<Skeleton />}>
    <Translator {...props} />
  </Suspense>
);

Text.propTypes = {
  translationKey: PropTypes.string.isRequired,
  variant: PropTypes.string,
  component: PropTypes.string,
};

Text.defaultProps = {
  variant: null,
  component: 'span',
};


export default Text;
