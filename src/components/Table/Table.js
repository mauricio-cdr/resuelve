import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import TableM from '@material-ui/core/Table';
import Pagination from '@material-ui/lab/Pagination';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHeadM from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

function TableHead(props) {
  const {
    classes, order, orderBy, onRequestSort,
  } = props;
  const { headers } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHeadM>
      <TableRow>
        {headers.map((headCell) => (
          <TableCell
            key={headCell.id}
            align="center"
            padding="default"
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHeadM>
  );
}

TableHead.propTypes = {
  classes: PropTypes.shape({
    visuallyHidden: PropTypes.string,
  }).isRequired,
  headers: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
};


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  tableBody: {
    minHeight: 261,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

function Table(props) {
  const { data } = props;
  const rows = data || [];
  const {
    page, totalPages, onChangePage, onClickInRow, headers,
  } = props;
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    onChangePage(newPage);
  };

  const renderCells = (row, headCells) => {
    if (!rows) return null;
    return headCells.map((h) => (
      <TableCell key={h.id} align="center">{`${h.format ? h.format(row[h.id]) : row[h.id]}`}</TableCell>
    ));
  };
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <TableM
            className={classes.table}
            aria-labelledby="tableTitle"
            size="medium"
          >
            <TableHead
              classes={classes}
              order={order}
              headers={headers}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody classes={{ root: classes.tableBody }}>
              {stableSort(rows, getComparator(order, orderBy))
                .map((row) => (
                  <TableRow
                    hover
                    tabIndex={-1}
                    key={row.name}
                    onClick={onClickInRow ? onClickInRow(row) : (a) => a}
                  >
                    {renderCells(row, headers)}
                  </TableRow>
                ))}
            </TableBody>
          </TableM>
        </TableContainer>
        <div style={{ padding: '5px 15px', justifyContent: 'flex-end', display: 'flex' }}>
          <Pagination count={totalPages} page={page} onChange={handleChangePage} color="primary" />
        </div>
      </Paper>
    </div>
  );
}

Table.propTypes = {
  page: PropTypes.number,
  data: PropTypes.arrayOf(PropTypes.shape({})),
  totalPages: PropTypes.number,
  onChangePage: PropTypes.func,
  onClickInRow: PropTypes.func,
  headers: PropTypes.arrayOf(PropTypes.shape({})),
};

Table.defaultProps = {
  page: 1,
  data: [],
  totalPages: 1,
  onChangePage: (a) => a,
  onClickInRow: (a) => a,
  headers: [],
};


export default Table;
