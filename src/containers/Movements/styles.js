import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
  container: {
    padding: '0px 15px',
    margin: '0px 15px',
  },
  title: {
    display: 'flex',
  },
  currencySection: {
    marginTop: '25px',
    maxHeight: '15px',
  },
});
