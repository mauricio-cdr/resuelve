
import {
  GET_MOVEMENTS_LIST_LOADING,
  GET_MOVEMENTS_LIST_SUCCESS,
  GET_MOVEMENTS_LIST_FAILED,
  GET_USD_MXN_EXCHANGE_LOADING,
  GET_USD_MXN_EXCHANGE_SUCCESS,
  GET_USD_MXN_EXCHANGE_FAILED,
} from '../../constants/action.types';
import { fetchMovementsByUser, fetchMyMovements } from '../../services/movements.service';
import { moneyExchange } from '../../services/money.service';

export const getMovements = (req) => ({
  types: [
    GET_MOVEMENTS_LIST_LOADING,
    GET_MOVEMENTS_LIST_SUCCESS,
    GET_MOVEMENTS_LIST_FAILED,
  ],
  promise: fetchMovementsByUser(req),
});

export const getMyMovements = (req) => ({
  types: [
    GET_MOVEMENTS_LIST_LOADING,
    GET_MOVEMENTS_LIST_SUCCESS,
    GET_MOVEMENTS_LIST_FAILED,
  ],
  promise: fetchMyMovements(req),
});

export const getMoneyExchange = (req) => ({
  types: [
    GET_USD_MXN_EXCHANGE_LOADING,
    GET_USD_MXN_EXCHANGE_SUCCESS,
    GET_USD_MXN_EXCHANGE_FAILED,
  ],
  promise: moneyExchange(req),
});
