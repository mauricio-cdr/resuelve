import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '../../components/Button';
import Table from '../../components/Table';
import Text from '../../components/Text';
import { formatDate } from '../../utils/date';
import { getMovements, getMyMovements, getMoneyExchange } from './movements.actions';
import useStyles from './styles';

const headCells = [
  { id: 'description', label: <Text translationKey="movements-page.tab-description" component="none" /> },
  { id: 'account', label: <Text translationKey="movements-page.tab-account" component="none" /> },
  { id: 'amount', label: <Text translationKey="movements-page.tab-amount" component="none" /> },
  { id: 'type', label: <Text translationKey="movements-page.tab-type" component="none" /> },
  { id: 'created_at', format: formatDate, label: <Text translationKey="movements-page.tab-created_at" component="none" /> },
];

export const MovementsPage = (props) => {
  const classes = useStyles();
  useEffect(() => {
    const { match: { params } } = props;
    props.getMoneyExchange({});
    if (params.user_id) {
      props.getMovements({ user_id: params.user_id });
      return;
    }
    props.getMyMovements({});
  }, []);

  const [currency, changeCurrency] = useState('mxn');

  function handleChangeExchange() {
    changeCurrency(currency === 'mxn' ? 'usd' : 'mxn');
  }

  function handleChangePage(page) {
    const { match: { params } } = props;
    const req = {
      params: { page },
    };
    if (params.user_id) {
      req.user_id = params.user_id;
      props.getMovements(req);
      return;
    }
    props.getMyMovements(req);
  }

  const calcWithChange = (exchange, mcurrency) => (v) => {
    if (currency !== 'mxn') {
      return `${Math.round(exchange.mxn2usd * v)} ${mcurrency}`;
    }
    return `${v} ${mcurrency}`;
  };

  const { movementsList: { records, pagination } } = props;
  const totalPages = pagination ? pagination.totalPages : 1;
  const page = pagination ? pagination.page : 1;
  const { exchange } = props;

  headCells[2].format = calcWithChange(exchange, currency);

  return (
    <div className={classes.container}>
      <div className={classes.title}>
        <Text translationKey="movements-page.title" variant="h1" component="h1" color="primary" />
        <div style={{ flex: 1 }} />
        <div className={classes.currencySection}>
          <div style={{ height: '10px' }} />
          <Button
            variant="outlined"
            onClick={handleChangeExchange}
            color="secondary"
          >
            {currency === 'mxn' ? 'MXN -> USD' : 'USD -> MXN'}
          </Button>
        </div>
      </div>
      <Table
        onClickInRow={(a) => a}
        data={records}
        totalPages={totalPages}
        page={page}
        headers={headCells}
        rowsPerPage={5}
        onChangePage={handleChangePage}
      />
    </div>
  );
};

MovementsPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  exchange: PropTypes.shape({}),
  getMoneyExchange: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      user_id: PropTypes.string,
    }),
  }).isRequired,
  getMovements: PropTypes.func.isRequired,
  getMyMovements: PropTypes.func.isRequired,
  movementsList: PropTypes.shape({
    records: PropTypes.arrayOf(
      PropTypes.shape({}),
    ),
    pagination: PropTypes.shape({
      totalPages: PropTypes.number,
      page: PropTypes.number,
    }),
  }),
};

MovementsPage.defaultProps = {
  movementsList: {},
  exchange: {},
};

const mapStateToProps = (state) => ({
  movementsList: state.get('movementsState').movementsList,
  isLoading: state.get('movementsState').isLoading,
  error: state.get('movementsState').error,
  exchange: state.get('movementsState').exchange,

});

const mapDispatchToProps = (dispatch) => ({
  getMovements: (req) => dispatch(getMovements(req)),
  getMyMovements: (req) => dispatch(getMyMovements(req)),
  getMoneyExchange: (req) => dispatch(getMoneyExchange(req)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MovementsPage);
