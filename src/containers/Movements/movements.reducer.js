import {
  GET_MOVEMENTS_LIST_LOADING,
  GET_MOVEMENTS_LIST_SUCCESS,
  GET_MOVEMENTS_LIST_FAILED,
  GET_USD_MXN_EXCHANGE_SUCCESS,
} from '../../constants/action.types';

const initialState = {
  isLoading: false,
  movementsList: {},
  error: null,
  exchange: {},
};

const movementsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MOVEMENTS_LIST_LOADING:
      return { ...state, isLoading: true, error: null };
    case GET_MOVEMENTS_LIST_SUCCESS: {
      return {
        ...state, isLoading: false, error: null, movementsList: action.result.data,
      };
    }
    case GET_MOVEMENTS_LIST_FAILED:
      return {
        ...state, isLoading: false, error: action.error,
      };
    case GET_USD_MXN_EXCHANGE_SUCCESS:
      return {
        ...state, exchange: action.result.data,
      };
    default:
      return state;
  }
};

export default movementsReducer;
