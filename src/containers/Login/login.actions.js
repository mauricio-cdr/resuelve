import {
  login as loginService,
  adminLogin as adminLoginService,
} from '../../services/login.service';
import { fogPass } from '../../utils/secure';

import {
  DO_LOGIN_LOADING,
  DO_LOGIN_FAILED,
  DO_LOGIN_SUCCESS,
  DO_LOG_OUT,
  LOAD_USER_INFO,
} from '../../constants/action.types';

export const login = (req) => {
  const pReq = { ...req, payload: { ...req.payload } };
  pReq.payload.password = fogPass(req.payload.password);
  return ({
    types: [
      DO_LOGIN_LOADING,
      DO_LOGIN_SUCCESS,
      DO_LOGIN_FAILED,
    ],
    promise: loginService(pReq),
  });
};

export const adminLogin = (req) => {
  const pReq = { ...req, payload: { ...req.payload } };
  pReq.payload.password = fogPass(req.payload.password);
  return ({
    types: [
      DO_LOGIN_LOADING,
      DO_LOGIN_SUCCESS,
      DO_LOGIN_FAILED,
    ],
    promise: adminLoginService(pReq),
  });
};

export const logout = () => ({
  type: DO_LOG_OUT,
});

export const loadUserInfo = () => ({
  type: LOAD_USER_INFO,
});
