import React from 'react';
import { shallow } from 'enzyme';
import { LoginPage } from '../LoginPage';

describe('<LoginPage />', () => {
  it('Should renders without crashing', () => {
    const component = shallow(
      <LoginPage
        location={{ pathname: '' }}
        history={{ push: (a) => a }}
        isLoading={false}
        login={(a) => a}
        adminLogin={(a) => a}
        error={null}
      />,
    );
    expect(component).toMatchSnapshot();
  });
});
