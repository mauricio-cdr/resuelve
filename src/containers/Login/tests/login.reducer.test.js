import reducer from '../login.reducer';
import {
  DO_LOGIN_LOADING,
  DO_LOGIN_FAILED,
} from '../../../constants/action.types';

describe('loginReducer', () => {
  const state = {
    isLoading: false,
    userInfo: {},
    error: null,
  };
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(state);
  });

  it('Should update loading state value to true', () => {
    const expectedState = {
      isLoading: true,
      userInfo: null,
      error: null,
    };
    const literalAction = {
      type: DO_LOGIN_LOADING,
    };
    expect(reducer(state, literalAction)).toEqual(expectedState);
  });

  it('Should update error state', () => {
    const expectedState = {
      isLoading: false,
      userInfo: null,
      error: { code: '500', msg: 'error' },
    };
    const literalAction = {
      type: DO_LOGIN_FAILED,
      error: { code: '500', msg: 'error' },
    };
    expect(reducer(state, literalAction)).toEqual(expectedState);
  });
});
