import {
  login,
} from '../login.actions';
import { login as doLogin } from '../../../services/login.service';
import {
  DO_LOGIN_LOADING,
  DO_LOGIN_SUCCESS,
  DO_LOGIN_FAILED,
} from '../../../constants/action.types';

describe('Film Detail actions', () => {
  it('Should create a getFilm action', () => {
    const expectedAction = {
      types: [
        DO_LOGIN_LOADING,
        DO_LOGIN_SUCCESS,
        DO_LOGIN_FAILED,
      ],
      promise: doLogin({ payload: {} }),
    };
    expect(login({ payload: {} })).toEqual(expectedAction);
  });
});
