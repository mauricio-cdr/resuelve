import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
  loginPage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    height: '100%',
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '250px',
    minWidth: '450px',
    justifyContent: 'space-between',
  },
  form: {
    minHeight: '300px',
  },
});
