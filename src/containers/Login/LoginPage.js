import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Input from '../../components/Input';
import Text from '../../components/Text';
import Button from '../../components/Button';
import { login, adminLogin } from './login.actions';
import { getUserSession } from '../../utils/session';
import useStyles from './styles';

export const LoginPage = (props) => {
  const classes = useStyles();

  const { location: { pathname } } = props;
  const isAdmin = pathname.indexOf('admin') > -1;

  function redirect(session) {
    const { history } = props;
    if (session.admin) {
      history.push('/users');
      return;
    }
    history.push('user/movements');
  }

  useEffect(() => {
    const session = getUserSession();
    if (session) {
      redirect(session);
    }
  }, []);

  const [creds, updateCreds] = useState({
    user: '',
    password: '',
  });

  function handleChange(e) {
    const { name, value } = e.target;
    updateCreds((c) => ({ ...c, [name]: value }));
  }

  function handleSubmit(e) {
    e.preventDefault();
    const req = { payload: creds };
    if (isAdmin) {
      props.adminLogin(req);
      return;
    }
    props.login(req);
  }

  const { isLoading, error } = props;
  return (
    <div className={classes.loginPage}>
      <section className="LoginPage">
        {(
          isAdmin
            ? <Text translationKey="login-page.admin-form-title" variant="h1" component="h1" />
            : <Text translationKey="login-page.user-form-title" variant="h1" component="h1" />
        )}
        <form className={classes.form} onSubmit={handleSubmit} noValidate autoComplete="off">
          <div className={classes.content}>
            <Input
              label={<Text translationKey="login-page.username-field-label" />}
              onChange={handleChange}
              name="user"
              value={creds.user}
              variant="outlined"
            />
            <Input
              label={<Text translationKey="login-page.password-field-label" />}
              type="password"
              name="password"
              value={creds.password}
              onChange={handleChange}
              variant="outlined"
            />
            {error && <Text color="error" translationKey="login-page.login-error-message" component="span" />}
            <Button variant="contained" type="submit" color="primary" disabled={isLoading}>
              <Text translationKey="login-page.button-form-message" component="none" />
            </Button>
            <div>
              {(
                isAdmin
                  ? (
                    <Link to="login">
                      <Text translationKey="login-page.are-you-user-link-label" />
                    </Link>
                  )
                  : (
                    <Link to="adminlogin">
                      <Text translationKey="login-page.are-you-admin-link-label" />
                    </Link>
                  )
              )}
            </div>
          </div>
        </form>
      </section>
    </div>
  );
};

LoginPage.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  isLoading: PropTypes.bool,
  login: PropTypes.func.isRequired,
  adminLogin: PropTypes.func.isRequired,
  error: PropTypes.shape({}),
};

LoginPage.defaultProps = {
  error: null,
  isLoading: false,
};

const mapStateToProps = (state) => ({
  userInfo: state.get('loginState').userInfo,
  isLoading: state.get('loginState').isLoading,
  error: state.get('loginState').error,
});

const mapDispatchToProps = (dispatch) => ({
  login: (req) => dispatch(login(req)),
  adminLogin: (req) => dispatch(adminLogin(req)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginPage);
