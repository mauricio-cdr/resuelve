import {
  DO_LOGIN_LOADING,
  DO_LOGIN_SUCCESS,
  DO_LOGIN_FAILED,
  DO_LOG_OUT,
  LOAD_USER_INFO,
} from '../../constants/action.types';
import { parseJWT } from '../../utils/secure';

const initialState = {
  isLoading: false,
  userInfo: {},
  error: null,
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case DO_LOGIN_LOADING:
      return { ...state, isLoading: true, userInfo: null };
    case DO_LOGIN_SUCCESS: {
      const { authorization } = action.result.headers;
      const jwt = authorization.split(' ')[1];
      const userInfo = parseJWT(jwt);
      localStorage.setItem('userInfo', jwt);
      window.location.reload();
      return {
        ...state, isLoading: false, error: null, userInfo,
      };
    }
    case DO_LOGIN_FAILED:
      return {
        ...state, isLoading: false, error: action.error, userInfo: null,
      };
    case DO_LOG_OUT: {
      localStorage.removeItem('userInfo');
      return { ...state, userInfo: null };
    }
    case LOAD_USER_INFO: {
      const jwt = localStorage.getItem('userInfo');
      const userInfo = jwt ? parseJWT(jwt) : {};
      return { ...state, userInfo };
    }
    default:
      return state;
  }
};

export default loginReducer;
