import {
  GET_USERS_LOADING,
  GET_USERS_SUCCESS,
  GET_USERS_FAILED,
} from '../../constants/action.types';
import { fetchUsers } from '../../services/users.service';

export const getUsers = (req) => ({
  types: [
    GET_USERS_LOADING,
    GET_USERS_SUCCESS,
    GET_USERS_FAILED,
  ],
  promise: fetchUsers(req),
});
