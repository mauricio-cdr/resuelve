
import {
  GET_USERS_LOADING,
  GET_USERS_SUCCESS,
  GET_USERS_FAILED,
} from '../../constants/action.types';

const initialState = {
  isLoading: false,
  usersList: {},
  error: null,
};

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS_LOADING:
      return { ...state, isLoading: true, error: null };
    case GET_USERS_SUCCESS: {
      return {
        ...state, isLoading: false, error: null, usersList: action.result.data,
      };
    }
    case GET_USERS_FAILED:
      return {
        ...state, isLoading: false, error: action.error,
      };
    default:
      return state;
  }
};

export default usersReducer;
