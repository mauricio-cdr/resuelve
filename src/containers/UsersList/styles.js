import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
  container: {
    padding: '0px 15px',
    margin: '0px 15px',
  },
  title: {
    margin: '5px 0px 35px 0px ',
  },
});
