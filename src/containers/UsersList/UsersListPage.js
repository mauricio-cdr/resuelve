import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Table from '../../components/Table';
import Text from '../../components/Text';
import { getUsers } from './users.actions';
import { formatDate } from '../../utils/date';

import useStyles from './styles';


const headCells = [
  { id: 'nombre', label: <Text translationKey="users-page.tab-nombre" component="none" />},
  { id: 'apellido', label: <Text translationKey="users-page.tab-apellido" component="none" />},
  { id: 'email', label: <Text translationKey="users-page.tab-email" component="none" /> },
  { id: 'active', format: (a) => (a ? 'active' : 'not active'), label: <Text translationKey="users-page.tab-active" component="none" /> },
  { id: 'created_at', format: formatDate, label: <Text translationKey="users-page.tab-created_at" component="none" /> },
];

export const UserListPage = (props) => {
  const classes = useStyles();
  useEffect(() => {
    props.getUsers({});
  }, []);

  function handleChangePage(page) {
    props.getUsers({ params: { page } });
  }

  const handleClickRow = (row) => () => {
    const { history } = props;
    history.push(`/users/${row.uid}/movements`);
  };

  const { usersList: { records, pagination } } = props;
  const totalPages = pagination ? pagination.totalPages : 1;
  const page = pagination ? pagination.page : 1;

  return (
    <div className={classes.container}>
      <div className={classes.title}>
        <Text translationKey="users-page.title" component="h1" variant="h1" color="primary" />
      </div>
      <Table
        onClickInRow={handleClickRow}
        data={records}
        headers={headCells}
        totalPages={totalPages}
        page={page}
        rowsPerPage={5}
        onChangePage={handleChangePage}
      />
    </div>
  );
};

UserListPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  getUsers: PropTypes.func.isRequired,
  usersList: PropTypes.shape({
    records: PropTypes.arrayOf(
      PropTypes.shape({}),
    ),
    pagination: PropTypes.shape({
      totalPages: PropTypes.number,
      page: PropTypes.number,
    }),
  }),
};

UserListPage.defaultProps = {
  usersList: {},
};

const mapStateToProps = (state) => ({
  usersList: state.get('usersState').usersList,
  isLoading: state.get('usersState').isLoading,
  error: state.get('usersState').error,
});

const mapDispatchToProps = (dispatch) => ({
  getUsers: (req) => dispatch(getUsers(req)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserListPage);
