import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  col: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  row: {
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  img: {
    margin: '100px',
    height: '300px',
  },
}));
