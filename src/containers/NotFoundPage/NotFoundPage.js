import React from 'react';
import useStyles from './styles';
import img from './404.png';

const NotFoundPage = () => {
  const classes = useStyles();
  return (
    <div className={classes.row}>
      <div className={classes.col}>
        <img src={img} className={classes.img} alt="not_found" />
      </div>
    </div>
  );
};

NotFoundPage.propTypes = {};


export default NotFoundPage;
