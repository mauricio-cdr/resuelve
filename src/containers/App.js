import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Toolbar from '../components/Toolbar';
import { darkTheme, lightTheme } from './Theme';
import { loadUserInfo } from './Login/login.actions';

import useStyles from './styles';

const App = (props) => {
  const { children } = props;
  const [isLightThemeSelected, changeTheme] = useState(true);
  const handleChangeTheme = () => changeTheme(!isLightThemeSelected);
  const classes = useStyles();
  const dispatch = useDispatch();
  useEffect(() => { dispatch(loadUserInfo()); }, []);
  return (
    <MuiThemeProvider theme={isLightThemeSelected ? lightTheme : darkTheme}>
      <div>
        <header>
          <Toolbar onChangeTheme={handleChangeTheme} isLightThemeSelected={isLightThemeSelected} />
        </header>
        <main className={classes.main}>
          {children}
        </main>
      </div>
    </MuiThemeProvider>
  );
};

App.propTypes = {
  children: PropTypes.element.isRequired,
};

export default App;
