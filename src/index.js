import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './i18n';
import './index.scss';
import Router from './routes';
import createStore from './store';
import * as serviceWorker from './serviceWorker';

require('dotenv').config();

const { store } = createStore();

ReactDOM.render(<Provider store={store}><Router /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
