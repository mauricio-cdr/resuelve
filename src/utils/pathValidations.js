import { getUserSession } from './session';

export const forLoggeUsers = () => {
  const session = getUserSession();
  if (!session) return false;
  return (!session.admin);
};

export const forLoggedAdmins = () => {
  const session = getUserSession();
  if (!session) return false;
  return (session.admin);
};
