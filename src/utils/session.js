import { parseJWT } from './secure';

export const getUserSession = () => {
  const jwt = localStorage.getItem('userInfo');
  if (!jwt) return null;
  return parseJWT(jwt);
};
