import md5 from 'blueimp-md5';

export const fogPass = (pass) => md5(pass);

export const parseJWT = (token) => {
  const segments = token.split('.');
  if (!(segments instanceof Array) || segments.length !== 3) {
    throw new Error('Invalid JWT');
  }
  const claims = segments[1];
  return JSON.parse(decodeURIComponent(escape(window.atob(claims))));
};
