import pruebaResuelveApi from './pruebaResuelveApi';

export const moneyExchange = (req) => pruebaResuelveApi.get(
  '/money/conversion', { params: req.params },
);
