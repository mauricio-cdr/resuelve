import pruebaResuelveApi from './pruebaResuelveApi';

export const fetchUsers = (req) => pruebaResuelveApi.get(
  'users/list', { params: req.params },
);
