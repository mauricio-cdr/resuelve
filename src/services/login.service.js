import pruebaResuelveApi from './pruebaResuelveApi';

export const login = (req) => pruebaResuelveApi.post(
  'users/login', req.payload, { params: req.params },
);
export const adminLogin = (req) => pruebaResuelveApi.post(
  'users/adminLogin', req.payload, { params: req.params },
);
