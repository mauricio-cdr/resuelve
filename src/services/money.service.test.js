import mockAxios from 'axios';
import {
  moneyExchange,
} from './money.service';

describe('accountService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should call get money conversions value', () => {
    const url = '/money/conversion';
    const req = { params: {} };
    moneyExchange(req);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
    expect(mockAxios.get).toHaveBeenCalledWith(
      url,
      { params: {} },
    );
  });
});
