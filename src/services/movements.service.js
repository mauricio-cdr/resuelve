import pruebaResuelveApi from './pruebaResuelveApi';

export const fetchMyMovements = (req) => pruebaResuelveApi.get(
  'users/myMovements', { params: req.params },
);

export const fetchMovementsByUser = (req) => pruebaResuelveApi.get(
  `/users/${req.user_id}/movements`, { params: req.params },
);
