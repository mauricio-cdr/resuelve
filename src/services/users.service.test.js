import mockAxios from 'axios';
import {
  fetchUsers,
} from './users.service';

describe('accountService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should call post property accounts', () => {
    const url = 'users/list';
    const req = {
      id: 1,
      params: {},
    };
    fetchUsers(req);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
    expect(mockAxios.get).toHaveBeenCalledWith(
      url,
      { params: {} },
    );
  });
});
