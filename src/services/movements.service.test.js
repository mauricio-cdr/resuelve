import mockAxios from 'axios';
import {
  fetchMyMovements,
} from './movements.service';

describe('accountService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should call post property accounts', () => {
    const url = 'users/myMovements';
    const req = {
      user_id: 1,
      params: {},
    };
    fetchMyMovements(req);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
    expect(mockAxios.get).toHaveBeenCalledWith(
      url,
      {
        params: {},
      },
    );
  });
});
