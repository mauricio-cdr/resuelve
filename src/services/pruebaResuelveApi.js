import axios from 'axios';

const requestAuthHandler = (request) => {
  const token = localStorage.userInfo;
  if (request.url.indexOf('login') === -1) {
    request.headers.Authorization = `Bearer ${token}`;
  }
  return request;
};

const http = () => {
  const baseURL = process.env.REACT_APP_PRUEBA_RESUELVE_API;
  const instance = axios.create({
    baseURL,
    headers: {
      'Content-Type': 'application/json',
    },
  });

  instance.interceptors.request.use(
    (request) => requestAuthHandler(request),
  );

  return instance;
};

export default http();
