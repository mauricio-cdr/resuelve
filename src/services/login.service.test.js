import mockAxios from 'axios';
import {
  login,
  adminLogin,
} from './login.service';

describe('accountService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should call post property accounts', () => {
    const url = 'users/login';
    const req = {
      id: 1,
      payload: {
        user: 'user',
        password: 'test',
      },
      params: {},
    };
    login(req);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
    expect(mockAxios.post).toHaveBeenCalledWith(
      url,
      {
        user: 'user',
        password: 'test',
      },
      {
        params: {},
      },
    );
  });

  it('should call post property accounts', () => {
    const url = 'users/adminLogin';
    const req = { params: {}, payload: { user: 'user', password: 'test' } };
    adminLogin(req);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
    expect(mockAxios.post).toHaveBeenCalledWith(
      url,
      { user: 'user', password: 'test' },
      { params: {} },
    );
  });
});
