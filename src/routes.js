import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import App from './containers/App';
import LoginPageC from './containers/Login/LoginPage';
import UsersPage from './containers/UsersList/UsersListPage';
import MovementsP from './containers/Movements/MovementsPage';
import NotFoundPage from './containers/NotFoundPage/NotFoundPage';
import { forLoggeUsers, forLoggedAdmins } from './utils/pathValidations';

const PrivateRoute = ({ component: Component, validation, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => (
        validation()
          ? <Component {...props} />
          : <Redirect to="/login" />
      )}
    />
  );
};

const router = () => (
  <Router>
    <App>
      <Switch>
        <Route exact path="/" component={LoginPageC} />
        <Route exact path="/login" component={LoginPageC} />
        <Route exact path="/adminlogin" component={LoginPageC} />
        <PrivateRoute exact validation={forLoggedAdmins} path="/users" component={UsersPage} />
        <PrivateRoute exact validation={forLoggedAdmins} path="/users/:user_id/movements" component={MovementsP} />
        <PrivateRoute exact validation={forLoggeUsers} path="/user/movements" component={MovementsP} />
        <Route path="**" component={NotFoundPage} />
      </Switch>
    </App>
  </Router>
);
export default router;
