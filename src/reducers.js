import {
  combineReducers,
} from 'redux-immutable';
import loginState from './containers/Login/login.reducer';
import usersState from './containers/UsersList/users.reducer';
import movementsState from './containers/Movements/movements.reducer';

export default combineReducers({
  loginState,
  usersState,
  movementsState,
});
