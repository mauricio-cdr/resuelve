const promiseMiddlware = () => (next) => (action) => {
  const { promise, types } = action;
  if (!promise) {
    return new Promise((resolve) => resolve(next(action)));
  }
  const [REQUEST, SUCCESS, FAILURE] = types;
  next({ type: REQUEST, promise });
  return promise.then(
    (result) => {
      next({ result, type: SUCCESS });
    },
    (error) => {
      next({ error, type: FAILURE });
    },
  );
};

export default promiseMiddlware;
